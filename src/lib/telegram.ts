import { LevelDB } from 'level';
import { Telegraf } from 'telegraf';
import { Env } from '../config/env';
import { DbKeys } from '../config/types';

/**
 * Telegram class.
 */
export class Telegram {
  public env: Env;
  public db: LevelDB;
  public client: Telegraf;

  /**
   * Class constructor.
   * @param env Environment variables.
   */
  public constructor(env: Env, db: LevelDB) {
    this.env = env;
    this.db = db;
    this.client = new Telegraf(env.telegramToken as string);
  }

  /**
   * Launches telegraf client.
   */
  public async launch(): Promise<Telegram> {
    this.commands();
    await this.client.launch();
    return this;
  }

  /**
   * Sends a message to all chats
   */
  public async sendMessage(message: string): Promise<void> {
    this.db.get(DbKeys.TELEGRAM_CHAT_ID, async (err, data) => {
      if (err) return;

      const telegramChatIds = JSON.parse(data);
      for (const id of telegramChatIds) {
        await this.client.telegram.sendMessage(id, message);
      }
    });
  }

  /**
   * Initiates Bot commands
   */
  private commands(): void {
    this.client.command('start', ctx => {
      this.db.get(DbKeys.TELEGRAM_CHAT_ID, async (err, data) => {
        let telegramChatIds: number[] = [];
        if (err) telegramChatIds = [];
        else telegramChatIds = JSON.parse(data);

        if (!telegramChatIds.find((e) => e == ctx.chat.id))
          telegramChatIds.push(ctx.chat.id);
        this.db.put(DbKeys.TELEGRAM_CHAT_ID, JSON.stringify(telegramChatIds));
      });
      return ctx.reply('Hey!');
    });

    this.client.command('stop', ctx => {
      this.db.get(DbKeys.TELEGRAM_CHAT_ID, async (err, data) => {
        let telegramChatIds: number[] = [];
        if (err) return;
        telegramChatIds = JSON.parse(data);

        telegramChatIds = telegramChatIds.filter((e) => e != ctx.chat.id);
        this.db.put(DbKeys.TELEGRAM_CHAT_ID, JSON.stringify(telegramChatIds));
      });
      return ctx.reply('Bye!');
    });

    // Enable graceful stop
    process.once('SIGINT', () => this.client.stop('SIGINT'));
    process.once('SIGTERM', () => this.client.stop('SIGTERM'));
  }
}
