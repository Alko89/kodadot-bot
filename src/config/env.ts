import * as dotenv from 'dotenv';

/**
 * Environment object interface.
 */
export interface Env {
  cronSchedule: string
  ipfsGateway: string
  subqueryUrl: string
  telegramToken: string
  discordToken: string
  twitterApiKey: string
  twitterApiSecret: string
  twitterAccessToken: string
  twitterAccessSecret: string
}

/**
 * Load variables from .env.
 */
dotenv.config();

/**
 * Cron schedule string.
 */
export const cronSchedule = process.env['CRON_SCHEDULE'] || '';

/**
 * IPFS gateway URL.
 */
export const ipfsGateway = process.env['IPFS_GATEWAY'] || '';

/**
 * Subquery URL.
 */
export const subqueryUrl = process.env['SUBQUERY_URL'] || '';

/**
 * Telegram token.
 */
export const telegramToken = process.env['TELEGRAM_TOKEN'] || '';

/**
 * Discord token.
 */
export const discordToken = process.env['DISCORD_TOKEN'] || '';

/**
 * Twitter api key.
 */
export const twitterApiKey = process.env['TWITTER_API_KEY'] || '';

/**
 * Twitter api secret.
 */
export const twitterApiSecret = process.env['TWITTER_API_SECRET'] || '';

/**
 * Twitter access token.
 */
export const twitterAccessToken = process.env['TWITTER_ACCESS_TOKEN'] || '';

/**
 * Twitter access secret.
 */
export const twitterAccessSecret = process.env['TWITTER_ACCESS_SECRET'] || '';
